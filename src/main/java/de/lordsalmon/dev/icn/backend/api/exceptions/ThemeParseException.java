package de.lordsalmon.dev.icn.backend.api.exceptions;

public class ThemeParseException extends Exception {
    public ThemeParseException(String msg) {
        super(msg);
    }
}
