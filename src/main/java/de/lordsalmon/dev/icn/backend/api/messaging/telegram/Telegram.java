package de.lordsalmon.dev.icn.backend.api.messaging.telegram;

import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import lombok.Getter;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import java.util.logging.Level;

public class Telegram {

    public static Telegram instance;

    @Getter
    private final TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
    @Getter
    private final ICNBot icnBot = new ICNBot();
    public static Telegram getInstance() {
        if (instance == null) {
            instance = new Telegram();
        }
        return instance;
    }

    private Telegram() {
        try {
            telegramBotsApi.registerBot(icnBot);
        } catch (TelegramApiRequestException e) {
            IcnAPI.getInstance(IcnAPI.class).getLogger().log(Level.WARNING, "The telegram bot could not be registered!");
        }
    }

}
