package de.lordsalmon.dev.icn.backend.api.utils;

import lombok.Getter;
import lombok.Setter;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple tool to create required JSON elements
 */
public class CallValidator {

    @Setter @Getter
    private List<String> validationList = new ArrayList<>();
    @Getter @Setter
    private String delimiter = "/"; //default is '/'

    public boolean valid = false;

    private boolean validate(JSONObject parent, String keyToCheck) {
        if (parent == null) {
            return false;
        }
        String[] splitKey = keyToCheck.split(delimiter);
        if (parent.has(splitKey[0])) {
            if (splitKey.length > 1) {
                try {
                    return validate(parent.getJSONObject(splitKey[0]), mergeRest(splitKey));
                } catch (Exception e) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public void start(JSONObject object) {
        for (String currentElement : validationList) {
            if (!validate(object, currentElement)) {
                valid = false;
                return;
            }
        }
        valid = true;
    }

    public boolean isValid() {
        return valid;
    }

    private String mergeRest(String[] splitKeys) {
        String out = "";
        for (int i = 1; i < splitKeys.length; i++) {
            out += splitKeys[i] + delimiter;
        }
        return out;
    }

}
