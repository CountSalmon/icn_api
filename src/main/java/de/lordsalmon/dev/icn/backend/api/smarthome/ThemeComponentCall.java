package de.lordsalmon.dev.icn.backend.api.smarthome;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.function.Consumer;

@ToString
@Accessors(chain = true)
public class ThemeComponentCall extends ComponentCall {

    @Getter
    private boolean finished = false;
    @Getter @Setter
    private String themeComponentCallId;
    @Getter @Setter
    private Theme themeParent;
    @Getter @Setter
    private boolean loop;
    @Getter @Setter
    private int sequence;
    @Getter @Setter
    private long delay;
    @Getter @Setter
    private boolean executeOnFinish;
    @Getter @Setter
    private Consumer<Integer> onFinish;
    @Getter @Setter
    private ColorAnimation animation;

}
