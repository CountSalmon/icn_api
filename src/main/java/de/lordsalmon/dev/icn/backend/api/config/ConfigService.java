package de.lordsalmon.dev.icn.backend.api.config;

import com.moandjiezana.toml.Toml;
import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import de.lordsalmon.dev.icn.backend.api.utils.Constants;
import de.lordsalmon.dev.icn.backend.api.utils.Utils;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;


@Accessors(chain = true)
public class ConfigService extends Toml {

    private static ConfigService instance;

    @Getter
    private Class parentClass;
    @Getter
    private File configFile;

    private ConfigService(Class parentClass) {
        this.parentClass = parentClass;
        //create cfg dir if not exists
        Utils.joinPath(Constants.cfgPath).mkdirs();
        configFile = Utils.joinPath(Constants.cfgPath, parentClass.getSimpleName() + ".toml");
        try {
            if (!configFile.exists()) {
                configFile.createNewFile();
            }
            this.parse(configFile);
        } catch (IOException e) {
            IcnAPI.getInstance(this.parentClass).getLogger().log(Level.WARNING, "config file could not be created!");
        }

    }

    public static ConfigService getInstance(Class parentClass) {
        if (instance == null || instance.getParentClass() != parentClass) {
            instance = new ConfigService(parentClass);
        }
        return instance;
    }


}
