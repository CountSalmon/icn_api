package de.lordsalmon.dev.icn.backend.api.utils;

import java.io.File;

public class Utils {

    public static File joinPath(String... pathElements) {
        StringBuilder fullPath = new StringBuilder(new File("").getAbsolutePath() + File.separator);
        for (String currentElement : pathElements.clone()) {
            fullPath.append(currentElement).append(File.separator);
        }
        return new File(fullPath.toString());
    }


}
