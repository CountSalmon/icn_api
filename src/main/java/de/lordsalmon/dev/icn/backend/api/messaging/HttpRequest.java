package de.lordsalmon.dev.icn.backend.api.messaging;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Intended to simplify the work with http
 */
@Accessors(chain = true)
@ToString
public class HttpRequest {

    @Getter private URL url;
    @Getter private HttpURLConnection httpURLConnection;
    @Getter private DataOutputStream dataOutputStream;
    @Getter private BufferedReader bufferedReader;
    @Getter @Setter private Map<String, String> parameters;
    @Getter @Setter private String method;


    public HttpRequest(String urlName) throws MalformedURLException {
        url = new URL(urlName);
    }

    /**
     * initializes all needed objects and executes the http request
     * @throws IOException
     */
    public void execute() throws IOException {
        httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod(method);
        httpURLConnection.setDoOutput(true);
        dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.writeBytes(QueryArgBuilder.getParameterString(parameters));
        bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
        dataOutputStream.flush();
        dataOutputStream.close();
    }

    /**
     * +
     * @return returns the result as a string
     * @throws IOException
     */
    public String getResult() throws IOException {
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = bufferedReader.readLine()) != null) {
            content.append(inputLine);
        }
        bufferedReader.close();
        return content.toString();
    }

    /**
     *
     * @return Only possible if the request is executed. If not it returns a NullPointer
     * @throws IOException
     */
    public int getResponseCode() throws IOException {
        return httpURLConnection.getResponseCode();
    }

    /**
     * embedded class to build the query args
     */
    private static class QueryArgBuilder {
        public static String getParameterString(Map<String, String> params) throws UnsupportedEncodingException {
            StringBuilder outBuilder = new StringBuilder();

            for(Map.Entry<String, String> entry: params.entrySet()) {
                outBuilder.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                outBuilder.append("=");
                outBuilder.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                outBuilder.append("&");
            }
            String out = outBuilder.toString();
            return out.length() > 0
                    ? out.substring(0, out.length() - 1)
                    : out;
        }
    }

}
