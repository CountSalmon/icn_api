package de.lordsalmon.dev.icn.backend.api.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;

/**
 * Intended to simplify the usage of the terminal e.g. to execute scripts of an other language
 */
@Accessors(chain = true)
public class Command {

    @Getter
    @Setter
    private String command;

    @Getter
    private final StringBuilder stringBuilder = new StringBuilder();
    @Getter
    private int exitValue;
    @Getter
    private BufferedReader bufferedReader;
    /* event when a new line is returned to the process */
    @Getter
    @Setter
    private Consumer<String> onResponseReceived;
    /* event when the command has finished */
    @Getter
    @Setter
    private Consumer<String> onCommandFinished;

    public Command(String commandContent) {
        command = commandContent;
    }

    public void execute() {
        try {
            if (command != null) {
                Process process = new ProcessBuilder(command).start();
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                    stringBuilder.append(System.lineSeparator());
                    onResponseReceived.accept(line);
                }
                exitValue = process.waitFor();
                onCommandFinished.accept(stringBuilder.toString());
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
