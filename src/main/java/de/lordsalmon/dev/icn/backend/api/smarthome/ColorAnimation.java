package de.lordsalmon.dev.icn.backend.api.smarthome;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Accessors(chain = true)
public class ColorAnimation {

    @Getter @Setter
    private String[] fromRGBA = new String[3];
    @Getter @Setter
    private String[] toRGB = new String[3];
    @Getter @Setter
    private long delay;

    public ColorAnimation() {}

}
