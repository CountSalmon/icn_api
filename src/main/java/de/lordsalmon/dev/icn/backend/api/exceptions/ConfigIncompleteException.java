package de.lordsalmon.dev.icn.backend.api.exceptions;

public class ConfigIncompleteException extends Exception {
    public ConfigIncompleteException(String msg) {
        super(msg);
    }
}
