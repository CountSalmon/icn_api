package de.lordsalmon.dev.icn.backend.api.smarthome;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
public class ComponentCall {

    @Getter @Setter
    private Component parent;
    @Getter @Setter
    private String call;

    private void executeCall() {

    }


}
