package de.lordsalmon.dev.icn.backend.api.messaging.telegram;

import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import lombok.Getter;
import lombok.Setter;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 */
public class TelegramCommand {

    public static List<TelegramCommand> registeredCommands = new ArrayList<>();

    @Getter
    public String[] invokes;
    @Getter @Setter
    public Consumer<Message> onExecute;

    public TelegramCommand() {
        registeredCommands.add(this);
    }

    public TelegramCommand setInvokes(String... aliases) {
        invokes = aliases.clone();
        return this;
    }

    public static void check(Message message) {
        if (isCommand(message)) {
            String commandInvoke = getMessageCommand(message);
            for (TelegramCommand telegramCommand : registeredCommands) {
                if (Arrays.asList(telegramCommand.getInvokes()).contains(commandInvoke)) {
                    telegramCommand.onExecute.accept(message);
                    break;
                }
            }
        }
    }

    private static boolean isCommand(Message message) {
        return message.getText().startsWith(
                IcnAPI.apiConfigService.getString("telegramPrefix")
        );
    }

    private static String getMessageCommand(Message message) {
        return message.getText().trim().split(" ")[0].toLowerCase();
    }

    private static String[] getMessageArgs(Message message) {
        List<String> messageSplit = Arrays.asList(message.getText().split(" "));
        messageSplit.remove(0);
        return (String[]) messageSplit.toArray();
    }

}