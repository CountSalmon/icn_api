package de.lordsalmon.dev.icn.backend.api;

import de.lordsalmon.dev.icn.backend.api.config.ConfigService;
import de.lordsalmon.dev.icn.backend.api.config.DataService;
import de.lordsalmon.dev.icn.backend.api.exceptions.ConfigIncompleteException;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.telegram.telegrambots.ApiContextInitializer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@ToString
@Accessors(chain = true)
public class IcnAPI {

    private static final List<IcnAPI> registeredInstances = new ArrayList<>();
    public static ConfigService apiConfigService = ConfigService.getInstance(IcnAPI.class);

    @Getter
    public static final String API_VERSION = "1.0.6";
    @Getter
    public static final String LATEST_MESSAGE = "setters are now chained";

    @Getter @Setter
    private ConfigService configService;
    @Getter @Setter
    private DataService dataService;
    @Getter @Setter
    private Logger logger;

    @Getter
    private Class parentClass;

    public IcnAPI() {}

    public IcnAPI(Class parentClass) {
        this.parentClass = parentClass;
    }

    public static IcnAPI getInstance(Class parentClass) {
        for (IcnAPI currentApi : registeredInstances) {
            if (currentApi.getParentClass() == parentClass) {
                return currentApi;
            }
        }
        IcnAPI icnAPI = new IcnAPI(parentClass);
        registeredInstances.add(icnAPI);
        return icnAPI;
    }

    /**
     * ensure that all requirements are satisfied
     * @throws ConfigIncompleteException
     */
    public void initTelegram() throws ConfigIncompleteException {
        ApiContextInitializer.init();
        if (apiConfigService.getString("telegramPrefix") == null) {
            throw new ConfigIncompleteException("Missing object: 'telegramPrefix'");
        }
    }
}
