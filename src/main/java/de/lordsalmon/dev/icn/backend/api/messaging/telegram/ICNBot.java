package de.lordsalmon.dev.icn.backend.api.messaging.telegram;

import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import lombok.Setter;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.function.Consumer;

public class ICNBot extends TelegramLongPollingBot {

    /**
     * execute when a telegram update is triggered
     * used to handle messages for different plugins
     */
    @Setter
    private Consumer<Update> onUpdateRecievedAction;

    public void onUpdateReceived(Update update) {
        if (onUpdateRecievedAction != null) {
            onUpdateRecievedAction.accept(update);
        }
    }

    public String getBotUsername() {
        return IcnAPI
                .apiConfigService
                .getTable("telegram")
                .getString("name");
    }

    public String getBotToken() {
        return IcnAPI
                .apiConfigService
                .getTable("telegram")
                .getString("token");
    }
}
