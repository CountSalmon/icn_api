package de.lordsalmon.dev.icn.backend.api.config;

import com.google.inject.internal.cglib.proxy.$LazyLoader;
import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import de.lordsalmon.dev.icn.backend.api.utils.Constants;
import de.lordsalmon.dev.icn.backend.api.utils.Utils;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataService {

    private static DataService instance;

    @Getter
    private Class parentClass;
    @Getter
    private File dataFile;
    private JSONObject object;

    private DataService(Class parentClass) {
        this.parentClass = parentClass;
        //create data dir if not exists
        Utils.joinPath(Constants.dataPath).mkdirs();
        dataFile = Utils.joinPath(Constants.dataPath, parentClass.getSimpleName() + ".json");
        try {
            if (!dataFile.exists()) {
                dataFile.createNewFile();
                FileWriter fileWriter = new FileWriter(dataFile);
                fileWriter.write("{}");  // makes it parsable for @code
                fileWriter.close();
            }
            object = new JSONObject(FileUtils.readFileToString(dataFile, "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getObject() throws IOException {
        object = new JSONObject(FileUtils.readFileToString(dataFile, "UTF-8"));
        return object;
    }

    public static DataService getInstance(Class parentClass) {
        if (instance == null || instance.getParentClass() != parentClass) {
            instance = new DataService(parentClass);
        }
        return instance;
    }
}