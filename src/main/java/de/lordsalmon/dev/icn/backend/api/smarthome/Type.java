package de.lordsalmon.dev.icn.backend.api.smarthome;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@ToString
@Accessors(chain = true)
public class Type {

    @Getter
    private static List<Type> registeredTypes = new ArrayList<>();

    @Getter @Setter
    private String name;

    public Type() {
        registeredTypes.add(this);
    }

}
