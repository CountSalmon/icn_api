package de.lordsalmon.dev.icn.backend.api.smarthome;

import de.lordsalmon.dev.icn.backend.api.exceptions.ThemeParseException;
import lombok.Getter;
import lombok.ToString;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.List;
import java.util.regex.Pattern;

@ToString
public class Theme {

   @Getter
   private List<ThemeComponentCall> themeComponentCalls = new ArrayList<ThemeComponentCall>();

   private String[] animationKeys = {"fromRGBA", "toRGBA", "duration"};
   private String[] themeKeys = {"delay", "sequence", "loop", "execOnFinish"};

   private final String rgbaRegex = "/^rgba?\\((\\d+),\\s*(\\d+),\\s*(\\d+)(?:,\\s*(\\d+(?:\\.\\d+)?))?\\)$/";
   private final Pattern rgbaPattern = Pattern.compile(rgbaRegex);

   public Theme(JSONObject themeFileContent) throws ThemeParseException {
      parse(themeFileContent);
   }

   private void parse(JSONObject themeFileContent) throws ThemeParseException {
      JSONArray calls = themeFileContent.getJSONArray("calls");
      for (Object theme : calls.toList()) {
         JSONObject themeObject = new JSONObject(theme.toString());
         if (isValidObject(themeObject, themeKeys)) {
            //raw data
            ThemeComponentCall currentThemeCall = new ThemeComponentCall()
                    .setDelay(themeObject.getLong("delay"))
                    .setSequence(themeObject.getInt("sequence"))
                    .setLoop(themeObject.getBoolean("loop"))
                    .setExecuteOnFinish(themeObject.getBoolean("execOnFinish"))
                    .setThemeParent(this);
            //animation
            if (themeObject.has("animation")) {

               JSONArray animationCalls = themeObject.getJSONArray("animation");
               for (Object currentAnimationCall : animationCalls.toList()) {
                  JSONObject currentAnimationObject = new JSONObject(currentAnimationCall.toString());
                  if (isValidObject(currentAnimationObject, animationKeys)) {
                     //set color animation
                     ColorAnimation currentColorAnimation = new ColorAnimation()
                             .setDelay(currentAnimationObject.getLong("delay"));
                     currentColorAnimation.setFromRGBA(
                             parseColor(currentAnimationObject.getString("fromRGB"))
                     );
                     currentColorAnimation.setToRGB(
                             parseColor(currentAnimationObject.getString("toRGB"))
                     );

                     currentThemeCall.setAnimation(currentColorAnimation);

                  } else {
                     throw new ThemeParseException("AnimationObject is not valid! (object: " + currentAnimationObject.toString() + ")");
                  }
               }
               themeComponentCalls.add(currentThemeCall);
            } else {
               throw new ThemeParseException("ThemeObject is not valid! (object: " + themeObject.toString() + ")");
            }

         }
      }
   }

   private boolean isValidObject(JSONObject object, String[] keys) {
      for (String currentKey : keys) {
         if (!object.has(currentKey)) {
            return false;
         }
      }
      return true;
   }

   private String[] parseColor(String content) throws ThemeParseException {
      if (matchesRGBA(content)) {
         content = content.replace("rgb(", "").replace(")", "");
         return content.split(", ");
      }
      throw new ThemeParseException("Could not parse RGB. (input: " + content + ")");
   }

   private boolean matchesRGBA(String content) {
      return rgbaPattern.matcher(content).matches();
   }

}