package de.lordsalmon.dev.icn.backend.api.smarthome;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.net.URL;
import java.util.Date;

@ToString
@Accessors(chain = true)
public class Component {

    @Getter @Setter
    private String id;
    @Getter @Setter
    private String type;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private String author;
    @Getter @Setter
    private URL thumbnailPath;
    @Getter @Setter
    private Date createdAt;
    @Getter @Setter
    private Date lastTimeUsed;
    @Getter
    @Setter
    private int timesUsed;


}
